from deepface import DeepFace
import os
import time
import pandas as pd
from PIL import Image

face_not_found=[]

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.heic', '.tiff')):
            filepath = os.path.join(folder, filename)
            images.append(filepath)
    return sorted(images)  # Sort the images by filename

def face_found_in_image(image):
    try:
        detections = DeepFace.extract_faces(image)
        return len(detections) > 0
    except Exception as e:
        print(f"Error detecting face in image: {e}")
        return False

def warmup():
    known_file = '/app/confusion_metrix/Portrait/2_portrait.jpg'
    unknown_file = '/app/confusion_metrix/Avatar/2_avatar.jpg'
    result = DeepFace.verify(img1_path=known_file, img2_path=unknown_file, model_name="Facenet512", detector_backend ="opencv", distance_metric="euclidean_l2", threshold =0.71)
    print('Warm up Finished')

def compare_faces(known_file, unknown_file):
    try:
        face_found_known = face_found_in_image(known_file)
        face_found_unknown = face_found_in_image(unknown_file)
        fnf_dir = '/app/output/FNF'
        os.makedirs(fnf_dir, exist_ok=True)

        if not face_found_known or not face_found_unknown:
            error_message = "Face not found in one or both images."
            if not face_found_known:
                error_message += f" Face not found in known image: {os.path.basename(known_file)}.\n"
                face_not_found.append(known_file)
            if not face_found_unknown:
                error_message += f" Face not found in unknown image: {os.path.basename(unknown_file)}.\n"
                face_not_found.append(unknown_file)
            
            # Get the base name of the file (i.e., '1_avatar.jpg')
            base_name = os.path.basename(known_file)
            # Split the base name at '_avatar'
            img_number = base_name.split('_portrait')[0]
            combined_image_name = f"{img_number}_portrait_avatar.jpg"

            combine_images(known_file, unknown_file, os.path.join(fnf_dir, combined_image_name))

            return {"error": error_message}

        start_time = time.time()
        # Compare faces using DeepFace with file paths
        result = DeepFace.verify(img1_path=known_file, img2_path=unknown_file, model_name="Facenet512", detector_backend ="opencv", distance_metric="euclidean_l2", threshold =0.59)

        end_time = time.time()
        execution_time = end_time - start_time

        result_data = {
            "matches": result["verified"],
            "distance_score": result["distance"],
            "threshold": result["threshold"],
            "execution_time": execution_time
        }
        print(f"Result for {os.path.basename(known_file)} vs {os.path.basename(unknown_file)}: {result_data} \n")
        return result_data
    except Exception as e:
        print(f"Error comparing faces: {e}")
        return {"error": "An error occurred during face comparison."}


def combine_images(image_path1, image_path2, output_path, layout='horizontal'):
    image1 = Image.open(image_path1)
    image2 = Image.open(image_path2)

    width1, height1 = image1.size
    width2, height2 = image2.size

    if layout == 'horizontal':
        combined_image = Image.new('RGB', (width1 + width2, max(height1, height2)))
        combined_image.paste(image1, (0, 0))
        combined_image.paste(image2, (width1, 0))
    else:
        combined_image = Image.new('RGB', (max(width1, width2), height1 + height2))
        combined_image.paste(image1, (0, 0))
        combined_image.paste(image2, (0, height1))

    combined_image.save(output_path)


def main():

    warmup()

    folder1 = '/app/confusion_metrix/Portrait'
    folder2 = '/app/confusion_metrix/Avatar'
    excel_file = '/app/confusion_metrix/customer_data.xlsx'  # Specify the path to your Excel file

    # Output directories
    fp_dir = '/app/output/FP'
    fn_dir = '/app/output/FN'
    os.makedirs(fp_dir, exist_ok=True)
    os.makedirs(fn_dir, exist_ok=True)

    # Load image pairs and ground truth from Excel file
    df = pd.read_excel(excel_file)

    total_distance = 0
    total_execution_time = 0
    total_matches = 0
    total_threshold = 0
    comparisons = 0
    successful_comparisons = 0
    failed_comparisons = 0
    true_matches = 0
    false_matches = 0
    # flag = 0
    y_true = []
    y_pred = []
    comparison_details = [] 
    false_noface_0 =0
    false_noface_1 =0

    for index, row in df.iterrows():
        img1 = os.path.join(folder1, row["Portrait"])
        img2 = os.path.join(folder2, row["Avatar"])
        truth = row['Truth']      
        result = compare_faces(img1, img2)

        # if flag == 0:
        #     flag += 1
        if "error" not in result:
            total_distance += result["distance_score"]
            total_execution_time += result["execution_time"]
            prediction = int(result["matches"])
            total_matches += prediction

            try:
                y_true_val = int(truth)
            except ValueError:
                if truth == "No Face Found":
                    y_true_val = "No Face Found"
                    if prediction == 0:
                        false_noface_0 +=1
                    else:
                        false_noface_1 +=1
                else:
                    print(f"Unexpected truth value: {truth}, skipping comparison.")
                    continue            

            # y_true_val = int(truth)  # True value from Excel (0 or 1)
            y_pred_val = 1 if result["matches"] else 0
            y_true.append(y_true_val)
            y_pred.append(y_pred_val)            

            combined_image_name = f"{row['Portrait']} + {row['Avatar']}"
            if y_true_val == 1 and y_pred_val == 0:
                # False Negative (FN)
                combine_images(img1, img2, os.path.join(fn_dir, combined_image_name))
            elif y_true_val == 0 and y_pred_val == 1:
                # False Positive (FP)
                combine_images(img1, img2, os.path.join(fp_dir, combined_image_name))
            elif y_true_val == "No Face Found":
                if y_pred_val == 1:
                    # No Face Found, but predicted as 1
                    print(f"{combined_image_name} is predicted 1 but it is No Face Found")
                elif y_pred_val == 0:
                    # No Face Found, but predicted as 0
                    print(f"{combined_image_name} is predicted 1 but it is No Face Found")

            if result["matches"]:
                true_matches += 1
            else:
                false_matches += 1
            if result["threshold"] is not None:
                total_threshold += result["threshold"]

            comparison_details.append((img1, img2, y_true_val, 'TRUE' if y_pred_val else 'FALSE'))
            comparisons += 1
            successful_comparisons += 1     
        
        else:
            failed_comparisons += 1
            comparison_details.append((img1, img2, "error", result['error']))
            print(f"Skipping comparison for {img1} vs {img2}: {result['error']}")
            print()

    if comparisons > 0:
        average_distance = total_distance / comparisons
        average_execution_time = total_execution_time / comparisons
        average_matches = total_matches / comparisons
        average_threshold = total_threshold / comparisons if total_threshold > 0 else None

        print(f"Average distance score: {average_distance}")
        print(f"Average execution time: {average_execution_time}")
        print(f"Average matches: {average_matches}")
        print(f"Average threshold: {average_threshold}")

        print(f"Total successful comparisons: {successful_comparisons}")
        print(f"Total failed comparisons: {failed_comparisons}")
        print(f"Total true matches: {true_matches}")
        print(f"Total false matches: {false_matches}")
        print()

        # # Calculate confusion matrix
        # tn, fp, fn, tp = confusion_matrix(y_true, y_pred).ravel()

        # print(f"Confusion Matrix:")
        # print(f"True Positives (TP): {tp}")
        # print(f"True Negatives (TN): {tn}")
        # print(f"False Positives (FP): {fp}")
        # print(f"False Negatives (FN): {fn}")

        # # Print classification report
        # print("\nClassification Report:")
        # print(classification_report(y_true, y_pred, target_names=["Not Same", "Same"]))

        # Calculate confusion matrix manually
        tp = sum((1 for true, pred in zip(y_true, y_pred) if true == 1 and pred == 1))
        tn = sum((1 for true, pred in zip(y_true, y_pred) if true == 0 and pred == 0))
        fp = sum((1 for true, pred in zip(y_true, y_pred) if true == 0 and pred == 1))
        fn = sum((1 for true, pred in zip(y_true, y_pred) if true == 1 and pred == 0))

        # Calculate accuracy, precision, recall, and F1-score
        accuracy = (tp + tn) / (tp + tn + fp + fn)
        precision = tp / (tp + fp) if (tp + fp) != 0 else 0
        recall = tp / (tp + fn) if (tp + fn) != 0 else 0
        f1_score = 2 * (precision * recall) / (precision + recall) if (precision + recall) != 0 else 0

        print(f"\nConfusion Matrix By Manual:")
        print(f"True Positives (TP)      : {tp}")
        print(f"True Negatives (TN)      : {tn}")
        print(f"False Positives (FP) *** : {fp}")
        print(f"False Negatives (FN) *   : {fn}")
        print("Classfication Matrix By Manual")
        print(f"Accuracy                                           : {accuracy:.4f}")
        print(f"Precision (Predicted TP from Model with prediction): {precision:.4f}")
        print(f"Recall (Actual TP from Truth with prediction)      : {recall:.4f}")
        print(f"F1-Score (Harmonic Mean)                           : {f1_score:.4f}")
        print()



if __name__ == "__main__":
    main()
