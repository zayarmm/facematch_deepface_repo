Confusion Metrix for 350 comparison
===================================
Dataset : Avatar folder vs Portrait folder
Script  : FP_FN_Merge_Evaluation_deepface.py

Updates in Scripts
==================
- Read images from Excel file : customer_data.xlsx
- Verify whether compared image are same or not
- Display results in Log
- Create folder of FP,FN,FNF with respective photos when the script is executed

Requirements
============
- Want to see the logs
- Want to see the created folders and photos in it
- Want to measures the resource usages
- Want to mount the model weight in the container
- Want to mount the dataset in the container
