from deepface import DeepFace
import numpy as np
import json

models = "Facenet512"
metrics = "euclidean_l2"
backends = "opencv"

img1_path = "tests\dataset\zayar\zayar1.jpg"
img2_path = "tests\dataset\mokham\MoKham1.jpg"

#face verification
verify_result = DeepFace.verify(
  img1_path = img1_path , 
  img2_path = img2_path,
  model_name = models,
  distance_metric = metrics,
  detector_backend = backends,
)
print(json.dumps(verify_result,indent=2))
