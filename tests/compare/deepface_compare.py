from deepface import DeepFace
import os
import time

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.gif', '.heic', '.tiff')):
            filepath = os.path.join(folder, filename)
            images.append(filepath)
    return sorted(images)  # Sort the images by filename

def face_found_in_image(image):
    try:
        detections = DeepFace.extract_faces(image)
        return len(detections) > 0
    except Exception as e:
        print(f"Error detecting face in image: {e}")
        return False

def compare_faces(known_file, unknown_file):
    try:
        face_found_known = face_found_in_image(known_file)
        face_found_unknown = face_found_in_image(unknown_file)

        if not face_found_known or not face_found_unknown:
            error_message = "Face not found in one or both images."
            if not face_found_known:
                error_message += f" Face not found in known image: {os.path.basename(known_file)}.\n"
            if not face_found_unknown:
                error_message += f" Face not found in unknown image: {os.path.basename(unknown_file)}.\n"
            return {"error": error_message}

        start_time = time.time()

        # Compare faces using DeepFace with file paths
        result = DeepFace.verify(img1_path=known_file, img2_path=unknown_file, model_name="Facenet512", distance_metric="euclidean_l2", threshold=0.8)
        threshold = result.get('threshold')  # Default threshold if not provided

        end_time = time.time()
        execution_time = end_time - start_time

        result_data = {
            "matches": result["verified"],
            "distance_score": result["distance"],
            "threshold": threshold,
            "execution_time": execution_time
        }
        print(f"Result for {os.path.basename(known_file)} vs {os.path.basename(unknown_file)}: {result_data} \n")
        return result_data
    except Exception as e:
        print(f"Error comparing faces: {e}")
        return {"error": "An error occurred during face comparison."}

def main():
    folder1 = '/app/compare/image1'
    folder2 = '/app/compare/image2'

    images1 = load_images_from_folder(folder1)
    images2 = load_images_from_folder(folder2)

    if len(images1) != len(images2):
        print("Folders do not contain the same number of images.")
        return

    total_distance = 0
    total_execution_time = 0
    total_matches = 0
    total_threshold = 0
    comparisons = 0
    successful_comparisons = 0
    failed_comparisons = 0
    true_matches = 0
    false_matches = 0
    flag = 0

    for img1, img2 in zip(images1, images2):
        result = compare_faces(img1, img2)
        if flag == 0:
            flag += 1
        elif "error" not in result:
            total_distance += result["distance_score"]
            total_execution_time += result["execution_time"]
            total_matches += int(result["matches"])
            if result["matches"]:
                true_matches += 1
            else:
                false_matches += 1
            if result["threshold"] is not None:
                total_threshold += result["threshold"]
            comparisons += 1 
            successful_comparisons += 1          
        
        else:
            failed_comparisons += 1

    if comparisons > 0:
        average_distance = total_distance / comparisons
        average_execution_time = total_execution_time / comparisons
        average_matches = total_matches / comparisons
        average_threshold = total_threshold / comparisons if total_threshold > 0 else None

        print(f"Average distance score: {average_distance}")
        print(f"Average execution time: {average_execution_time}")
        print(f"Average matches: {average_matches}")
        print(f"Average threshold: {average_threshold}")

    print(f"Total successful comparisons: {successful_comparisons}")
    print(f"Total failed comparisons: {failed_comparisons}")
    print(f"Total true matches: {true_matches}")
    print(f"Total false matches: {false_matches}")

if __name__ == "__main__":
    main()
